package game;

import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import render.Rend;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import render.RendTerrain;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author segonzal
 */

public class Renderer {
    private static final String windowTitle = "BlueHyperCube";
    private boolean closeRequested = false;
    private final Controller controller;
    private static Renderer self;
    
    private Rend rend;
    public Camera camera;
    
    private Renderer(){
        controller = Controller.getInstance();
        rend = new RendTerrain();
        camera = new Camera();
    }
    
    /**
     * Returns an instance of a Renderer, which creates and updates the window.
     * The Renderer contains te mainloop.
     * @return The Renderer instance.
     */
    public static Renderer getInstance(){
        if (self==null)
            self = new Renderer();
        return self;
    }
    
    /**
     * Main body, initialization, loop and clean up.
     */
    public void run(){
        createWindow();
        initGL();
        
        rend.CreateVBO();
        //main loop
        while (!closeRequested){
            Time.tick();
            pollInput();
            renderGL();
            Display.update();
        }
        cleanUp();
    }
    
    /**
     * Initialize OpenGL.
     */
    private void initGL(){
        int width = Display.getDisplayMode().getWidth();
        int height = Display.getDisplayMode().getHeight();

        GL11.glViewport(0, 0, width, height);
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        
        GLU.gluPerspective(45.0f, ((float) width / (float) height), 0.1f, 100.0f);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDepthFunc(GL11.GL_LEQUAL);
        GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
                
        GL11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
        GL11.glEnableClientState(GL11.GL_NORMAL_ARRAY);
	GL11.glEnableClientState(GL11.GL_COLOR_ARRAY);
        
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glShadeModel(GL11.GL_SMOOTH);
        GL11.glEnable(GL11.GL_COLOR_MATERIAL);
        GL11.glClearDepth(1.0f);
        
        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glEnable(GL11.GL_LIGHT0);

        GL11.glMaterial(GL11.GL_FRONT, GL11.GL_SPECULAR, floatBuffer(1.0f, 1.0f, 1.0f, 1.0f));
        GL11.glMaterialf(GL11.GL_FRONT, GL11.GL_SHININESS, 50.0f);

        GL11.glLight(GL11.GL_LIGHT0, GL11.GL_POSITION, floatBuffer(0.0f, 2500.0f, 0.0f, 0.0f));
        GL11.glLight(GL11.GL_LIGHT0, GL11.GL_AMBIENT, floatBuffer(0.5f, 0.5f, 0.5f, 1.0f));
        GL11.glLight(GL11.GL_LIGHT0, GL11.GL_DIFFUSE, floatBuffer(1.0f, 1.0f, 1.0f, 1.0f));

        GL11.glLightModel(GL11.GL_LIGHT_MODEL_AMBIENT, floatBuffer(0.1f, 0.1f, 0.1f, 1.0f));
    }
    
      public FloatBuffer floatBuffer(float a, float b, float c, float d) {
    float[] data = new float[]{a,b,c,d};
    FloatBuffer fb = BufferUtils.createFloatBuffer(data.length);
    fb.put(data);
    fb.flip();
    return fb;
  }
    
    /**
     * Render OpenGL.
     */
    private void renderGL(){
        
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
        GL11.glLoadIdentity(); // Reset The View
        
        camera.lookThrough();
        
        rend.DrawVBO();
    }
       
    /**
     * clean up resources.
     */
    private void cleanUp(){
        Display.destroy();
    }
    
    /**
     * Poll Input.
     */
    private void pollInput(){
        controller.pollInput(this);
    }

    /**
     * Creates the window.
     */
    private void createWindow(){
        try {
            Display.setDisplayMode(new DisplayMode(1024, 800));
            Display.setVSyncEnabled(true);
            Display.setResizable(true);
            Display.setTitle(windowTitle);
            Display.create();
        } catch (LWJGLException e) {
            Sys.alert("Error", "Initialization failed!\n\n" + e.getMessage());
            System.exit(0);
        }
    }
    
    /**
     * Requesting close.
     */
    public void closeWindow(){
        this.closeRequested = true;
    }
    
    public static void main(String[] args) {
        Renderer game = Renderer.getInstance();
        game.run();
    }
}