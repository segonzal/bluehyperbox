package game;

/**
 *
 * @author seba
 */
public class Time {
    private static long pastTime;
    private static long elapsed;
    private static Time self;
    
    /**
     * Manages the time.
     */
    private Time(){
        pastTime=System.currentTimeMillis();
        elapsed=0;
    }
    
    /**
     * For lazy initialization.
     */
    private static void lazyInitialization(){
        if(self==null)
            self=new Time();
    }
    
    /**
     * Calculates the elapsed time between calls to tick().
     * @see #tick()
     * @return The elapsed time in milliseconds.
     */
    public static long getElapsedTime(){
        lazyInitialization();
        return elapsed;
    }
    
    /**
     * Upates the elapsed time.
     */
    public static void tick(){
        lazyInitialization();
        long actual = System.currentTimeMillis();
        elapsed = actual-pastTime;
        pastTime = actual;
    }
}
