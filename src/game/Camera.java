package game;

import org.lwjgl.opengl.GL11;

/**
 *
 * @author seba
 */
public class Camera {
    private float x,y,z,yaw,pitch;
    
    /**
     * A camera that can be moved through the scene.
     */
    public Camera(){
        x=y=-25;
        z=-98;
    }
    /**
     * Change the y axis rotation angle.
     * @param dyaw Amount of change.
     */
    public void yaw(float dyaw){
        yaw+=dyaw;
    }
    /**
     * Change the x axis rotation angle.
     * @param dpitch Amount of change.
     */
    public void pitch(float dpitch){
        pitch+=dpitch;
    }
    /**
     * Moves the camera forward.
     * @param distance How much it moved.
     */
    public void walkForward(float distance){
        x-= distance * (float)Math.sin(Math.toRadians(yaw));
        z+= distance * (float)Math.cos(Math.toRadians(yaw));
    }
    /**
     * Moves the camera forwards.
     * @param distance How much it moved.
     */
    public void walkBackwards(float distance){
        x+= distance * (float)Math.sin(Math.toRadians(yaw));
        z-= distance * (float)Math.cos(Math.toRadians(yaw));
    }
    /**
     * Moves the camera to the left, "surrounding".
     * @param distance How much it moved.
     */
    public void strafeLeft(float distance){
        x -= distance * (float)Math.sin(Math.toRadians(yaw-90));
        z += distance * (float)Math.cos(Math.toRadians(yaw-90));
    }
    /**
     * Moves the camera to the right, "surrounding".
     * @param distance How much it moved.
     */
    public void strafeRight(float distance){
        x -= distance * (float)Math.sin(Math.toRadians(yaw+90));
        z += distance * (float)Math.cos(Math.toRadians(yaw+90));
    }
    /**
     * Moves the camera up.
     * @param distance How much it moved.
     */
    public void moveUp(float distance){
        y -= distance;
    }
    /**
     * Moves the camera down.
     * @param distance How much it moved.
     */
    public void moveDown(float distance){
        y += distance;
    }
    /**
     * Look through the camera.
     */
    public void lookThrough(){
        //roatate the pitch around the X axis
        GL11.glRotatef(pitch, 1.0f, 0.0f, 0.0f);
        //roatate the yaw around the Y axis
        GL11.glRotatef(yaw, 0.0f, 1.0f, 0.0f);
        //translate to the position vector's location
        GL11.glTranslatef(x, y, z);
    }
}
