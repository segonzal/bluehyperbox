package game;


import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author segonzal
 */
class Controller {
    private static Controller self;
    private final float mouseSensitivity = 0.15f;
    private final float movementSpeed = 0.005f;
    
    private Controller(){
    }
    
    /**
     * Returns a single instance of the controller.
     * @return 
     */
    public static Controller getInstance(){
        if (self==null){
            self = new Controller();
        }
        return self;
    }

    /**
     * Polls the user input.
     * @param aRenderer Renderer instance.
     */
    void pollInput(Renderer aRenderer) {
        Camera camera = aRenderer.camera; 
        
        //distance in mouse movement from the last getDX() call.
        float dx = Mouse.getDX();
        //distance in mouse movement from the last getDY() call.
        float dy = Mouse.getDY();

        if (Mouse.isButtonDown(1)){
            //control camera yaw from x movement from the mouse
            camera.yaw(dx * mouseSensitivity);
            //control camera pitch from y movement from the mouse
            camera.pitch(-dy * mouseSensitivity);
        }
            
        // scroll through key events
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE)
                    aRenderer.closeWindow();
            }
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_W))
            camera.walkForward(movementSpeed*Time.getElapsedTime());
        if (Keyboard.isKeyDown(Keyboard.KEY_S))
            camera.walkBackwards(movementSpeed*Time.getElapsedTime());
        if (Keyboard.isKeyDown(Keyboard.KEY_A))
            camera.strafeLeft(movementSpeed*Time.getElapsedTime());
        if (Keyboard.isKeyDown(Keyboard.KEY_D))
            camera.strafeRight(movementSpeed*Time.getElapsedTime());
        if (Keyboard.isKeyDown(Keyboard.KEY_SPACE))
            camera.moveUp(movementSpeed*Time.getElapsedTime());
        if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT))
            camera.moveDown(movementSpeed*Time.getElapsedTime());
        

        if (Display.isCloseRequested()) {
            aRenderer.closeWindow();
        }
        
    }
}
