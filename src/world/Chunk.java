package world;

import java.util.Random;
import render.Renderizable;

/**
 *
 * @author seba
 */
public class Chunk implements Renderizable{
    static final int CHUNK_SIZE = 8;
    static final int CHUNK_HEIGHT = 8;
    
    private static final float CHUNK_LENGTH = AbstractVoxel.MAX_VOXEL_SIZE*CHUNK_SIZE;
    
    private AbstractVoxel[][][] voxels;
    private float[] vertexDataArray;
    private boolean hasChanged = true;
    private int nmbVert;
    
    private Chunk nextX,nextZ,pastX,pastZ;
    private float x,y,z;
    
    /**
     * A Chunk is a big arrangement of voxels.
     * @param x Chunk's lowest x coordinate.
     * @param y Chunk's lowest y coordinate.
     * @param z Chunk's lowest z coordinate.
     */
    public Chunk(float x,float y,float z){
        this.x=x;
        this.y=y;
        this.z=z;
        generateChilds();
    }
    
    public Chunk setNextX(){
        nextX = new Chunk(x+CHUNK_LENGTH,y,z);
        return nextX;
    }
    public Chunk setNextZ(){
        nextZ = new Chunk(x,y,z+CHUNK_LENGTH);
        return nextZ;
    }
    public Chunk setPastX(){
        pastX = new Chunk(x-CHUNK_LENGTH,y,z);
        return pastX;
    }
    public Chunk setPastZ(){
        pastZ = new Chunk(x,y,z-CHUNK_LENGTH);
        return pastZ;
    }
    
    public Chunk[] getNeighbors(){
        Chunk[] n = {this,nextX,nextZ,pastX,pastZ};
        return n;
    }

    /**
     * Returns an array of coordinates of the chunk's voxel's vertex positions.
     * @param adepth Maximum depth of search.
     * @return This chunk's voxel's vertex coordinates.
     */
    @Override
    public float[] getVertexDataArray(int adepth) {
        if (!hasChanged) return vertexDataArray;
        vertexDataArray = new float[getNumbOfCoordinates(adepth)];
        float size = AbstractVoxel.MAX_VOXEL_SIZE;
        int s=0;
        for(int i=0;i<CHUNK_SIZE;i++){
            for(int j=0;j<CHUNK_HEIGHT;j++){
                for(int k=0;k<CHUNK_SIZE;k++){
                    AbstractVoxel voxel = voxels[i][j][k];
                    if (!voxel.isSolid()) continue;
                    float sx = x + i*size;
                    float sy = y + j*size;
                    float sz = z + k*size;
                    float[] v = voxel.getVertexDataArray(sx, sy, sz,adepth);
                    System.arraycopy(v,0, vertexDataArray, s, v.length);
                    s+=v.length;
                }
            }
        }
        hasChanged = false;
        return vertexDataArray;
    }


    /**
     * Returns the number of elements on a vertex data array.
     * @param adepth Maximum depth of search.
     * @return Number of elements on a vertex array.
     */
    @Override
    public int getNumbOfCoordinates(int adepth) {
        if (!hasChanged) return nmbVert;
        nmbVert=0;
        for(int i=0;i<CHUNK_SIZE;i++){
            for(int j=0;j<CHUNK_HEIGHT;j++){
                for(int k=0;k<CHUNK_SIZE;k++){
                    AbstractVoxel voxel = voxels[i][j][k];
                    if (!voxel.isSolid())continue;
                    nmbVert+=voxel.getNumbOfCoordinates(adepth);
                }
            }
        }
        return nmbVert;
    }

    /**
     * Creates the voxels of the chunk.
     */
    private void generateChilds() {
        voxels = new AbstractVoxel[CHUNK_SIZE][CHUNK_HEIGHT][CHUNK_SIZE];
        Random r = new Random();
        for(int i = 0;i<CHUNK_SIZE;i++){
            for(int k = 0;k<CHUNK_SIZE;k++){
                int max = r.nextInt(CHUNK_HEIGHT);
                for(int j = 0;j<max;j++)
                    voxels[i][j][k] = new NodeVoxel(getRandomType(r));
                for(int j=max;j<CHUNK_HEIGHT;j++)
                    voxels[i][j][k] = new LeafVoxel(BlockType.DEFAULT,0);
            }
        }
    }
    
    private BlockType getRandomType(Random r){
        switch(r.nextInt(6)){
            case 0:
                return BlockType.DIRT;
            case 1:
                return BlockType.GRASS;
            case 2:
                return BlockType.SAND;
            case 3:
                return BlockType.STONE;
            case 4:
                return BlockType.WATER;
            default:
                return BlockType.WOOD;
        }
    }
}
