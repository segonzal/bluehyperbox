package world;

/**
 *
 * @author seba
 */
public class NodeVoxel extends AbstractVoxel{
    private AbstractVoxel[] childs;
    
    /**
     * Creates a refinable voxel.
     * @param atype Type of block.
     * @param adepth Depth of the tree.
     */
    private NodeVoxel(BlockType atype, int adepth) {
        super(atype, adepth);
        generateChilds();
    }
    
    /**
     * Creates a refinable voxel of depth 0.
     * @param atype Type of block.
     */
    public NodeVoxel(BlockType atype){
        this(atype,0);
    }
    
    /**
     * Retruns the length of the voxel's edge.
     * @return Length of the voxel's edge.
     */
    @Override
    public float getSize() {
        return 2*childs[0].getSize();
    }

    private static final int[][] cube ={{0,0,0},{1,0,0},{1,0,1},{0,0,1},{0,1,0},{1,1,0},{1,1,1},{0,1,1}};
    
    /**
     * Given the value of the lowest coordinate of the cube, returns the vertex
     * array of the voxel's childs.
     * @param x Lowest x coordinate value of the voxel.
     * @param y Lowest y coordinate value of the voxel.
     * @param z Lowest z coordinate value of the voxel.
     * @param adepth Maximum depth of search.
     * @return 3 coordinate vertex array, of every face of the voxel's childs. 
     */
    @Override
    public float[] getVertexDataArray(float x, float y, float z, int adepth) {
        if (depth>=adepth){
            return super.getVertexDataArray(x, y, z, adepth);
        }
        float[] vertex = new float[getNumbOfCoordinates(adepth)*3];
        float size = getSize()/2;
        int s=0;
        for(int i=0;i<8;i++){
            AbstractVoxel child = childs[i];
            if(!child.isSolid()) continue;
            float sx = x + (cube[i][0])*size;
            float sy = y + (cube[i][1])*size;
            float sz = z + (cube[i][2])*size;
            float[] v = child.getVertexDataArray(sx, sy, sz, adepth);
            System.arraycopy(v,0, vertex, s, v.length);
            s+=v.length;
        }
        return vertex;
    }

    /**
     * Creates the childs of the node.
     */
    private void generateChilds() {
        childs = new AbstractVoxel[8];
        for(int i=0;i<8;i++){
            if (depth<MAX_DEPTH-1)
                childs[i] = new NodeVoxel(type,depth+1);
            else
                childs[i] = new LeafVoxel(type,depth+1);
        }
    }
    
    @Override
     public int getNumbOfCoordinates(int depth){
         int s=0;
         for(AbstractVoxel child:childs)
             s+=child.getNumbOfCoordinates(depth);
         return s;
     }

    @Override
    public boolean isSolid() {
        return true;
    }
}
