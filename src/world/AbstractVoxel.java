package world;

import render.Renderizable;

/**
 *
 * @author seba
 */
public abstract class AbstractVoxel implements Renderizable{
    
    public static final float MAX_VOXEL_SIZE = 2;
    public static final int MAX_DEPTH = 2;

    protected final BlockType type;
    protected final int depth;
    
    public AbstractVoxel(BlockType atype,int adepth){
        type=atype;
        depth=adepth;
    }
    /**
     * Gets the voxel's edge size.
     * @return Length of the Voxel's edge side.
     */
    public float getSize(){
            return MAX_VOXEL_SIZE/(float)Math.pow(2,depth);
    }
    
    /**
     * Returns the number of elements on a vertex data array.
     * @param adepth Maximum depth of search.
     * @return Number of elements on a vertex array.
     */
    @Override
    public float[] getVertexDataArray(int adepth){
        return getVertexDataArray(0,0,0,adepth);
    }
    
    /**
     * Given the value of the lowest coordinate of the cube, returns the vertex
     * data array of the voxel.
     * @param x Lowest x coordinate value of the voxel.
     * @param y Lowest y coordinate value of the voxel.
     * @param z Lowest z coordinate value of the voxel.
     * @param adepth Maximum depth of search.
     * @return Vertex data array following: x y z nx ny nz r g b alpha. 
     */
    public float[] getVertexDataArray(float x, float y, float z, int adepth){
        float size = getSize();
        float[] color = getColor();
        float[] vertex = {
             // front face
            x     , y     , z     , 0, 0, 1,color[0],color[1],color[2],color[3],
            x+size, y     , z     , 0, 0, 1,color[0],color[1],color[2],color[3],
            x+size, y     , z+size, 0, 0, 1,color[0],color[1],color[2],color[3],
            x     , y     , z+size, 0, 0, 1,color[0],color[1],color[2],color[3],
            // back face
            x     , y+size, z     , 0, 0,-1,color[0],color[1],color[2],color[3],
            x+size, y+size, z     , 0, 0,-1,color[0],color[1],color[2],color[3],
            x+size, y+size, z+size, 0, 0,-1,color[0],color[1],color[2],color[3],
            x     , y+size, z+size, 0, 0,-1,color[0],color[1],color[2],color[3],
            // right face
            x+size, y     , z     , 1, 0, 0,color[0],color[1],color[2],color[3],
            x+size, y+size, z     , 1, 0, 0,color[0],color[1],color[2],color[3],
            x+size, y+size, z+size, 1, 0, 0,color[0],color[1],color[2],color[3],
            x+size, y     , z+size, 1, 0, 0,color[0],color[1],color[2],color[3],
            // left face
            x     , y     , z     ,-1, 0, 0,color[0],color[1],color[2],color[3],
            x     , y+size, z     ,-1, 0, 0,color[0],color[1],color[2],color[3],
            x     , y+size, z+size,-1, 0, 0,color[0],color[1],color[2],color[3],
            x     , y     , z+size,-1, 0, 0,color[0],color[1],color[2],color[3],
            // top face
            x     , y     , z+size, 0, 1, 0,color[0],color[1],color[2],color[3],
            x+size, y     , z+size, 0, 1, 0,color[0],color[1],color[2],color[3],
            x+size, y+size, z+size, 0, 1, 0,color[0],color[1],color[2],color[3],
            x     , y+size, z+size, 0, 1, 0,color[0],color[1],color[2],color[3],
            // bottom face
            x     , y     , z     , 0,-1, 0,color[0],color[1],color[2],color[3],
            x+size, y     , z     , 0,-1, 0,color[0],color[1],color[2],color[3],
            x+size, y+size, z     , 0,-1, 0,color[0],color[1],color[2],color[3],
            x     , y+size, z     , 0,-1, 0,color[0],color[1],color[2],color[3]
        };
        return vertex;
    }
    
    /**
     * Returns the color asociated with the type.
     * @return A color array.
     */
    private float[] getColor(){
        float[] color = {0.0f,0.0f,0.0f,1.0f};
        switch(type){
            case GRASS:
                color[1]=1.0f;
                color[3]=1.0f;
                break;
            case DIRT:
                color[0]=1.0f;
                color[1]=1.0f;
                break;
            case WATER:
                color[2]=1.0f;
                color[3]=0.5f;
                break;
            case STONE:
                color[0]=0.5f;
                color[1]=0.5f;
                color[2]=0.5f;
                break;
            case WOOD:
                color[0]=0.5f;
                color[1]=0.5f;
                break;
            case SAND:
                color[0]=0.5f;
                color[2]=0.5f;
                break;
            case DEFAULT:
            default:
                break;
        }
        return color;
    }
    
    /**
     * Returns the number of vertex on a leaf Voxel.
     * @param depth Maximum depth of search.
     * @return Number of vertex.
     */
    @Override
    public int getNumbOfCoordinates(int depth) {
        return 6*4*10;
    }

    /**
     * Check if the voxel is solid.
     * @return True if the voxel is solid.
     */
    public abstract boolean isSolid();
}