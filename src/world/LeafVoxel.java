package world;

/**
 *
 * @author seba
 */
public class LeafVoxel extends AbstractVoxel {
    
    /**
     * Creates a Voxel with no childs.
     * @param atype Tipe of the Voxel.
     * @param adepth Depth of the leaf.
     */
    public LeafVoxel(BlockType atype, int adepth) {
        super(atype,adepth);
    }

    @Override
    public boolean isSolid() {
        switch(type){
            case GRASS:
            case DIRT:
            case WATER:
            case STONE:
            case WOOD:
            case SAND:
                return true;
            case DEFAULT:
            default:
                return false;
        }
    }
}
