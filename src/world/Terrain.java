/*
 * Here comes the text of your license
 * Each line should be prefixed with  * 
 */

package world;

import render.Renderizable;

/**
 *
 * @author seba
 */
public class Terrain implements Renderizable{
    private static Terrain self;
    private Chunk currentChunk;
    private Terrain(){
        currentChunk = new Chunk(0,0,0);
        currentChunk.setNextX();
        currentChunk.setNextZ();
        currentChunk.setPastX();
        currentChunk.setPastZ();
    }
    public static Terrain getInstance(){
        if(self==null)self=new Terrain();
        return self;
    }

    @Override
    public float[] getVertexDataArray(int adepth) {
        Chunk[] neighbors = currentChunk.getNeighbors();
        float[] terrain = new float[getNumbOfCoordinates(adepth)];
        int s=0;
        for (Chunk chunk : neighbors) {
            float[] v = chunk.getVertexDataArray(adepth);
            System.arraycopy(v,0, terrain, s, v.length);
            s+=v.length;
        }
        return terrain;
    }

    @Override
    public int getNumbOfCoordinates(int adepth) {
        Chunk[] neighbors = currentChunk.getNeighbors();
        int h=0;
        for (Chunk neighbor : neighbors) {
            h += neighbor.getNumbOfCoordinates(adepth);
        }
        return h;
    }
}
