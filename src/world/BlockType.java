package world;

/**
 *
 * @author seba
 */
public enum BlockType {
    DEFAULT,GRASS, DIRT, WATER, STONE, WOOD, SAND
}
