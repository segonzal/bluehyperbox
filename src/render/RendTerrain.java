/*
 * Here comes the text of your license
 * Each line should be prefixed with  * 
 */

package render;

import world.Terrain;

/**
 *
 * @author seba
 */
public class RendTerrain extends Rend{

    public RendTerrain() {
        super(Terrain.getInstance());
    }
    
}
