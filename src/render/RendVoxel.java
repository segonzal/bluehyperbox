package render;

import world.BlockType;
import world.LeafVoxel;

/**
 *
 * @author seba
 */
public class RendVoxel extends Rend {

    public RendVoxel() {
        super(new LeafVoxel(BlockType.DEFAULT,0));
    }
}
