package render;

/**
 *
 * @author seba
 */
public interface Renderizable {
    /**
     * Returns a vertex data array.
     * The returned arrays follows: x y z nx ny nz r g b alpha components.
     * @param adepth Maximum depth of search.
     * @return Vertex coordinate array.
     */
    public float[] getVertexDataArray(int adepth);
    
    /**
     * Returns the number of elements on a vertex data array.
     * @param adepth Maximum depth of search.
     * @return Number of elements on a vertex array.
     */
    public abstract int getNumbOfCoordinates(int adepth);
}
