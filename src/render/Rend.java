package render;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

/**
 *
 * @author seba
 */
public abstract class Rend {
    private Renderizable r;
    private int vertex_buffer_id;
    private int array_size;
    
    public Rend(Renderizable re){
        r=re;
    }
    /**
     * Draws the Vertex Buffer Object.
     */
    public final void DrawVBO() {
        //number of components, Type, byte offset, starting point(bytes)
        GL11.glVertexPointer(3, GL11.GL_FLOAT, 40, 0);
        GL11.glNormalPointer(GL11.GL_FLOAT, 40, 12);
        GL11.glColorPointer(4, GL11.GL_FLOAT, 40, 24);

        GL11.glDrawArrays(GL11.GL_QUADS, 0, array_size/ 10);
        GL11.glPopMatrix();
    }
    
    /**
     * Creates the Vertex Buffer Object.
     */
    public final void CreateVBO() {
        IntBuffer buffer = BufferUtils.createIntBuffer(1);
        GL15.glGenBuffers(buffer);
        vertex_buffer_id = buffer.get(0);
        
         // x y z nx ny nz r g b alpha
        float[] vertex_data_array = r.getVertexDataArray(0);
        array_size=vertex_data_array.length;
                
        FloatBuffer vertex_buffer_data = BufferUtils.createFloatBuffer(array_size);
        vertex_buffer_data.put(vertex_data_array);
        vertex_buffer_data.flip();

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vertex_buffer_id);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertex_buffer_data, GL15.GL_STATIC_DRAW);
    }
}
